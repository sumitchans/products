import csv
import pandas as pd

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.conf import settings

from .models import *


def add_update_products(products_data, request):
    """
    function to add product in the table
    :param products_data:
    :param request:
    :return:
    """
    new_product_list = []
    updated_product_list = []
    insert_batch_count = 1
    update_batch_count = 1
    for product_data in products_data:
        try:
            product = ProductInfo.objects.get(sku=product_data.get("sku"))
            product.name = product_data.get("name", product.name)
            product.desc = product_data.get("description", product.desc)
            updated_product_list.append(product)
            update_batch_count = update_batch_count + 1
        except ObjectDoesNotExist as ex:
            print("product with sku" + product_data["sku"] + "does not exist")
            product = ProductInfo(name=product_data["name"], sku=product_data["sku"], desc=product_data["description"])
            new_product_list.append(product)
            insert_batch_count = insert_batch_count + 1
        if insert_batch_count and insert_batch_count % settings.INSERT_BATCH_SIZE == 0:
            try:
                ProductInfo.objects.bulk_create(new_product_list)
            except Exception as ex:
                print(repr(ex))
            new_product_list = []
        if update_batch_count and update_batch_count % settings.UPDATE_BATCH_SIZE == 0:
            try:
                ProductInfo.objects.bulk_update(updated_product_list, fields=["name", "desc"])
            except Exception as ex:
                print(repr(ex))
            updated_product_list = []
    return True


def load_products(file_path, request):
    """
    Function to read csv and load products in the database
    :param file_path:
    :param request:
    :return:
    """
    data = pd.read_csv(file_path)
    data.drop_duplicates(subset="sku", keep='last', inplace=True)
    data = data.to_dict("records")
    return add_update_products(data, request)


def get_product_object(pk, request):
    """
    Function to get product info base on the product Id or SKU
    :param pk: pk can be id or sku of the product
    :param request:
    :return: product object
    """
    if pk.isdigit():
        query = Q(id=pk)
    elif isinstance(pk, str):
        query = Q(sku=pk)
    else:
        raise Exception("pk values is not correct")
    try:
        product = ProductInfo.objects.get(query)
        return product
    except:
        raise Exception("product does not exist")

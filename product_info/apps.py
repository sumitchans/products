from django.apps import AppConfig


class ProductInfoConfig(AppConfig):
    name = 'product_info'

import requests
import json

from django.conf import settings


class ApiService:
    """Custom service calls"""

    @staticmethod
    def get(url, params):
        """
        params:
        url: relative url
        params: dict of values to be sent
        media(Boolean): if True then request is made with url param
        without appending backend host.default=False
        """
        url = settings.BACKEND_SITE_HOST + url
        response = requests.get(url, params=params)
        return response

    @staticmethod
    def post(url, params, headers={}, files={}):
        """
        params:
        url: relative url
        params: dict of values to be sent
        media(Boolean): if True then request is made with url param
        without appending backend host.default=False
        """
        url = settings.BACKEND_SITE_HOST + url
        response = requests.post(url=url, json=params, headers=headers, files=files)
        return response

    @staticmethod
    def patch(url, params, headers={}):
        """
        params:
        url: relative url
        params: dict of values to be sent
        media(Boolean): if True then request is made with url param
        without appending backend host.default=False
        """
        url = settings.BACKEND_SITE_HOST + url + "/"
        response = requests.patch(url=url, data=params, headers=headers)
        return response

    @staticmethod
    def delete(url):
        """
        params:
        url: relative url
        params: dict of values to be sent
        media(Boolean): if True then request is made with url param
        without appending backend host.default=False
        """
        url = settings.BACKEND_SITE_HOST + url + "/"
        response = requests.delete(url=url)
        return response


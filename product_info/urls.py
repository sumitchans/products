from django.conf.urls import url, include
from rest_framework import routers

from .views import *

router = routers.DefaultRouter()
router.register(r'products', ProductInfoView, basename='products')
router.register(r'webhook', AddWebHookView, basename='webhook')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^load-product/$', UploadProductFile.as_view(), name='load-product'),
    url(r'check/$', CheckUrl.as_view(), name='check-url'),
    url(r'^products-list/$', ProductPageView.as_view(), name='product-list'),
    url(r'^products-delete/(?P<product_id>[-\w]+)$', ProductDeleteView.as_view(), name='product-delete'),
    url(r'^products-info/(?P<product_id>[-\w]+)$', ProductPageInfoView.as_view(), name='product-info'),
    url(r'^webhook-info/$', WebhookPageView.as_view(), name='webhook-info'),
    url(r'^file-upload/$', FileUploadPageView.as_view(), name='file-upload')
]
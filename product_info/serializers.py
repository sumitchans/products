from rest_framework.serializers import Serializer, ModelSerializer

from .models import ProductFile, ProductInfo, WebHook


class FileUploadSerializer(ModelSerializer):
    """
    Serialize class to upload the product file by the user
    """
    class Meta:
        model = ProductFile
        exclude = ('created_at', 'modified_at', 'is_deleted')
        read_only_fields = ("id", )


class ProductInfoSerializer(ModelSerializer):
    """
    Serializer class to add , update , get product info
    """

    class Meta:
        model = ProductInfo
        exclude = ('created_at', 'modified_at', 'is_deleted')
        read_only_fields = ("id",)


class WebHookSerializer(ModelSerializer):
    """
    Serializer class to add , update , get webhook
    """

    class Meta:
        model = WebHook
        exclude = ('created_at', 'modified_at', 'is_deleted')
        read_only_fields = ("id",)

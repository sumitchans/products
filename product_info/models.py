import datetime
import requests
import json

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
from django.conf import settings


class ModelBase(models.Model):
    """
    This is a abstract model class to add is_deleted, created_at and modified at fields in any model
    """
    is_deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class ProductInfo(ModelBase):
    """
    Model class to store information about the products
    """
    name = models.CharField(max_length=255)
    sku = models.CharField(max_length=255, unique=True, null=False)
    desc = models.TextField()
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = "product_info"
        ordering = ["-id"]


class ProductFile(ModelBase):
    """
    Model to store uploaded file information
    """
    file = models.FileField(upload_to='product_files')

    class Meta:
        db_table = "product_file"


class WebHook(ModelBase):
    _event_types = (
        ('created', 'created'),
        ('updated', 'updated')
    )
    url = models.URLField()
    event = models.CharField(choices=_event_types, max_length=255)

    class Meta:
        db_table = 'web_hook'


@receiver(post_save, sender=ProductInfo)
def send_data_to_webhook(sender, instance, created, **kwagrs):
    from .serializers import ProductInfoSerializer
    serializer = ProductInfoSerializer(instance)
    if created:
        query = models.Q(event="created")
    else:
        query = models.Q(event="updated")
    urls = WebHook.objects.filter(query).values_list("url", flat=True)
    params = {"timestamp": datetime.datetime.timestamp(datetime.datetime.now())}
    params.update(serializer.data)
    for url in urls:
        try:
            requests.post(url, data=json.dumps(params), headers={'content-type': 'application/json'})
        except Exception as ex:
            print(repr(ex))



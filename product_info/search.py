import re
from django.db.models import Q
from django.contrib.postgres.search import SearchVector, SearchQuery

from .models import ProductInfo




def get_product_query_set(query_params, request):
    """
    Function to get query set of product info
    :param query_params:
    :param request:
    :return:
    """
    search_vector = SearchVector("name") + SearchVector("sku")
    query = None
    if "is_active" in query_params:
        query = Q(is_active=query_params.get("is_active"))
    if "query" in query_params:
        search_query = SearchQuery(query_params.get("query").lower())
        query = query & Q(search=search_query) if query else Q(search=search_query)
    if query:
        result = ProductInfo.objects.annotate(search=search_vector).filter(query)
    else:
        result = ProductInfo.objects.all()
    return result

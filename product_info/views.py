from django.shortcuts import render

# Create your views here.
from rest_framework.viewsets import views, ViewSet, ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_201_CREATED
from rest_framework.exceptions import APIException
from django.shortcuts import redirect
from django.views import View
from .api_services import ApiService

from .serializers import *
from .utils import *
from .search import *
from products.celery_app import task_queue_processor


class UploadProductFile(APIView):
    """
    Class to upload product file
    """
    def post(self, request):
        """
        Post method to receive request from the user for uploading the file
        :param request:
        :return:
        """
        params = request.data
        serializer = FileUploadSerializer(data=params)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            response = serializer.data
            task_queue_processor.delay({"file_path": response["file"]}, 'load_products')
            return Response(data=serializer.data, status=HTTP_201_CREATED)
        else:
            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)
        return APIException("something went wrong. Please contact Us @999999999")


class ProductInfoView(ModelViewSet):
    """
    Class to create, get, update the product info
    """
    serializer_class = ProductInfoSerializer

    def list(self, request):
        """
        return list of the product based on the user query
        :param request:
        :return:
        """
        query_params = dict()
        if "is_active" in request.GET:
            query_params["is_active"] = request.GET.get("is_active")
        if "query" in request.GET:
            query_params["query"] = request.GET.get("query")
        query_set = get_product_query_set(query_params, request)
        page = self.paginate_queryset(query_set)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data,)
        serializer = self.get_serializer(query_set, many=True)
        return Response(data=serializer.data, status=HTTP_200_OK)

    def retrieve(self, request, pk=None):
        """
        Function to get one product info based on the pk
        :param request:
        :param pk: pk can be id or sku of the product
        :return:
        """
        try:
            serializer = self.get_serializer(get_product_object(pk, request))
            return Response(data=serializer.data, status=HTTP_200_OK)
        except Exception as ex:
            raise APIException(str(ex))

    def update(self, request, pk=None):
        """
        Function to update the product info base on the pk
        :param request:
        :param pk: pk can be id or sku of the product
        :return:
        """
        try:
            serializer = self.get_serializer(get_product_object(pk, request), data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(data=serializer.data, status=HTTP_200_OK)
            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)
        except Exception as ex:
            raise APIException(str(ex))

    def partial_update(self, request, pk=None):
        """
        Function to partial update the product using patch method
        :param request:
        :param pk: pk can be id or sku of the product
        :return:
        """
        try:
            serializer = self.get_serializer(get_product_object(pk, request), data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(data=serializer.data, status=HTTP_200_OK)
            return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)
        except Exception as ex:
            raise APIException(str(ex))

    def destroy(self, request, pk=None):
        """
        Function to delete the product
        :param request:
        :param pk: pk can be id or sku of the product
        :return:
        """
        try:
            if pk != '-1':
                product = get_product_object(pk, request)
                product.delete()
            else:
                ProductInfo.objects.all().delete()
            return Response(data="product deleted successfully")
        except Exception as ex:
            raise APIException(str(ex))


class CheckUrl(APIView):
    """
    View to check API
    """
    def post(self, request):
        data = request.data
        print(data)
        return Response(data, status=HTTP_200_OK)


class AddWebHookView(ModelViewSet):
    queryset = WebHook.objects.all()
    serializer_class = WebHookSerializer


class ProductPageView(View):
    template_name = 'products.html'

    def get(self, request, *args, **kwargs):
        data = request.GET
        params = {}
        if data.get("query"):
            params["query"] = data['query']
        if data.get("is_active"):
            params["is_active"] = data["is_active"]
        if data.get("limit"):
            params["limit"] = data.get("limit")
        if data.get("offset"):
            params["offset"] = data.get("offset")
        response = ApiService.get('/products', params)
        context_data = {}
        if response.status_code == 200:
            response = response.json()
            next = response["next"]
            if next:
                next_dt = next.split("?")
                context_data["next"] = next_dt[-1]
            previous = response["previous"]
            if previous:
                prev_dt = previous.split("?")
                context_data["previous"] = prev_dt[- 1]
            context_data["product_list"] = response["results"]
        return render(request, self.template_name, context_data)

    def post(self, request):
        data = request.POST
        params = {"name": data.get("product_name"), "sku": data.get("product_sku"), "desc": data.get("product_desc")}
        response = ApiService.post('/products/', params)
        context_data = {}
        if response.status_code == 201:
            return redirect(settings.BACKEND_SITE_HOST+"/products-info/" + response.json()['sku'])
        return render(request, self.template_name, context_data)


class ProductPageInfoView(View):
    template_name = 'product_info.html'

    def get(self, request, product_id):
        response = ApiService.get('/products/'+ product_id, {})
        context_data = {}
        if response.status_code == 200:
            context_data = {"product": response.json()}
        return render(request, self.template_name, context_data)

    def post(self, request, product_id):
        data = request.POST
        params = {"name": data.get("product_name"),
                  "sku": data.get("product_sku"),
                  "desc": data.get("product_desc"),
                  "is_active": data.get("is_active")}
        response = ApiService.patch('/products/' + product_id, params)
        context_data = {}
        if response.status_code == 200:
            return redirect(settings.BACKEND_SITE_HOST + "/products-info/" + response.json()['sku'])
        return render(request, self.template_name, context_data)


class ProductDeleteView(View):
    template_name = 'delete.html'

    def get(self, request, product_id):
        ApiService.delete('/products/' + product_id)
        return render(request, self.template_name)


class WebhookPageView(View):
    template_name = 'webhook.html'

    def get(self, request):
        response = ApiService.get('/webhook', {})
        context_data = {}
        if response.status_code == 200:
            context_data = {"webhook_list": response.json()["results"]}
        return render(request, self.template_name, context_data)

    def post(self, request):
        data = request.POST
        params = {"url": data.get("webhook_url"), "event": data.get("webhook_event")}
        response = ApiService.post('/webhook/', params)
        context_data = {}
        if response.status_code in [201, 200]:
            return redirect(settings.BACKEND_SITE_HOST+"/webhook-info")
        return render(request, self.template_name, context_data)


class FileUploadPageView(View):
    template_name = 'file_upload.html'

    def get(self, request):
        return render(request, self.template_name, {})

    def post(self, request):
        data = request.POST
        params = {"url": data.get("webhook_url"), "event": data.get("webhook_event")}
        response = ApiService.post('/webhook/', params)
        context_data = {}
        if response.status_code in [201, 200]:
            return redirect(settings.BACKEND_SITE_HOST+"/webhook-info")
        return render(request, self.template_name, context_data)




# import datetime
#
# from haystack import indexes
#
# from .models import ProductInfo
#
#
# class ProductInfoIndex(indexes.SearchIndex, indexes.Indexable):
#     """
#     Class for product info search
#     """
#     text = indexes.CharField(Document=True, use_template=True)
#     name = indexes.CharField(model_attr="name")
#     sku = indexes.CharField(model_attr="sku")
#     desc = indexes.CharField(model_attr="desc")

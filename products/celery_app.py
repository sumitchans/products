import os

from celery import Celery

from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'products.settings')

app = Celery("task")
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)


@app.task(bind=True, default_retry_delay=3 * 60)
def task_queue_processor(self, params, func):
    from product_info.utils import load_products
    countdown = 180
    max_retries = 3
    try:
        if func == 'load_products':
            load_products(params['file_path'], None)
    except Exception as exc:
        raise self.retry(exc=exc, countdown=countdown, max_retries=max_retries)
